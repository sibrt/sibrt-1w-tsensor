#!/usr/bin/env python3
import os
import sys
from sibrtclient import  SleepAbsolut, Log
from sibrtclient.client import Client
from sibrtclient.onewire import OneWire


def parse_cli():
    import argparse
    parser = argparse.ArgumentParser(description='Read data from DS18B20+ Sensor and send it to SiBrT server')
    parser.add_argument('--id',dest='id', help='Sensor ID', required=True)
    parser.add_argument('--server', dest='server', default="127.0.0.1:5000",help='SiBrT Server address (default: 127.0.0.1:5000 )')
    parser.add_argument('--value', dest='value', help='value to send in dummy mode')
    parser.add_argument('--sensor', dest='sensor', help='Primary sensor 1wire ID')
    parser.add_argument('--fallback', dest='fallback', help='Fallback sensor 1wire ID')
    parser.add_argument('--time', dest='time', type=int, default=1, help='Refresh sensor data every N seconds (default 1) ')
    return parser.parse_args()



if __name__ == "__main__":
    args=parse_cli()
    log=Log()
    temp=0.0
    sensor_ctr=0
    sensors=[]
    if(args.sensor!=None):
        sensors.append(args.sensor)
    if(args.fallback!=None):
        sensors.append(args.fallback)
    client=Client(server=args.server)
    stream=client.ConnectStream(args.id)
    timer=SleepAbsolut()
    log.level=Log.Warn
    if(args.sensor!=None):
        ow=OneWire()
    while True:
        sucess=False
        if(len(sensors)==0):
            log.W("Running in Dummy-Mode")
            ## dummy mode
            if(args.value==None):
                temp+=0.1
            else:
                temp=int(args.value)
            sucess=True
        else:
            try:
                log.D("Read Sensor <%s>",sensors[sensor_ctr])
                temp=ow.ReadTemperature(sensors[sensor_ctr])
                sucess=True
            except:
                log.W("Read Sensor <%s> failed",sensors[sensor_ctr])
        if(sucess==True):
            stream.Send(temp)
        else:
            sensor_ctr+=1
            if(sensor_ctr>=len(sensors)):
                sensor_ctr=0
        timer.Sleep(args.time)
