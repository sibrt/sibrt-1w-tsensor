.PHONY: clean dist
python=python3
VERSION="0.1"
BUILD=$(shell date +%Y%m%d%H%M)
NAME=sibrt-1w-tsensor
PKG=${NAME}-${VERSION}-${BUILD}
DPKG_ARCH:=armhf
DPKG=dpkg-deb -Zgzip


all:

dist: release/${PKG}.deb

build/${PKG}:
	mkdir -p $@/usr/lib/systemd/system/
	mkdir -p $@/etc/default/
	mkdir -p $@/usr/share/${NAME}/sibrtclient
	cp -r sibrtclient/__init__.py $@/usr/share/${NAME}/sibrtclient/
	cp -r sibrtclient/onewire.py $@/usr/share/${NAME}/sibrtclient/
	cp -r sibrtclient/client.py $@/usr/share/${NAME}/sibrtclient/
	cp 1w-tsensor.py $@/usr/share/${NAME}/1w-tsensor.py
	chmod 755 $@/usr/share/${NAME}/1w-tsensor.py
	cp dist/${NAME}@.service $@/usr/lib/systemd/system/
	mkdir -p $@/DEBIAN
	echo 'test -e /etc/default/${NAME}@temp1 || echo \#ARGS= --sensor AABBCCDD --fallback AABBCC --server 127.0.0.1:5000 > /etc/default/${NAME}@temp1' > $@/DEBIAN/postinst
	echo "systemctl daemon-reload" >> $@/DEBIAN/postinst
	chmod 755 $@/DEBIAN/postinst
	cp dist/dpkg-control $@/DEBIAN/control
	echo Version: ${VERSION}-${BUILD} >> $@/DEBIAN/control
	echo Architecture: ${DPKG_ARCH} >> $@/DEBIAN/control


release/${PKG}.deb: build/${PKG}
	mkdir -p release
	${DPKG} -b $< $@

clean:
	rm -rf build
